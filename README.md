# Project info
The game of my authorship. Game implemented in Java using [libGDX](https://libgdx.badlogicgames.com/index.html) library. The main goal of the game is to kill the appropriate balls that are drawn in the menu.

# Project objectives
- Learn basics of libGdx.
- Learn how to write simple 2D. 
- Learn how to print objects textures on the screen and how to manage these resources in Java code.

**Controls:**
- Mouse (Making balls)
- ESC (Exit from game)
- P (Pause)

# Project insight
![App](/misc/MainMenu.png)
![App](/misc/Highscore.png)
![App](/misc/Instructions.png)
![App](/misc/Lottery.png)
![App](/misc/Preferences.png)
![App](/misc/Game.png)
