package com.my.game;

import com.badlogic.gdx.physics.box2d.*;

import java.util.Random;

class BodyFactory {

    private static BodyFactory thisInstance;
    private static World world;

    private BodyFactory(World world) {
        BodyFactory.world = world;
    }

    public static BodyFactory getInstance(World world){
        if(thisInstance == null){
            thisInstance = new BodyFactory(world);
        }
        return thisInstance;
    }

    static Body makeCirclePolyBody() {
        BodyDef boxBodyDef = new BodyDef();
        boxBodyDef.type = BodyDef.BodyType.DynamicBody;
        Random random = new Random();
        boxBodyDef.position.x = -17 + random.nextInt(33);
        boxBodyDef.position.y = -8 + random.nextInt(20);
        boxBodyDef.fixedRotation = false;

        Body boxBody = world.createBody(boxBodyDef);
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(2);
        boxBody.createFixture(makeFixture(circleShape));
        circleShape.dispose();
        return boxBody;
    }

    private static FixtureDef makeFixture(Shape shape){
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 1f;
        fixtureDef.friction = 0f;
        fixtureDef.restitution = 1f;
        return fixtureDef;
    }
}

