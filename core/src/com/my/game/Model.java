package com.my.game;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.my.game.controller.Dialogs;
import com.my.game.enums.State;
import com.my.game.loader.ModelAssetManager;
import com.my.game.views.MainScreen;

import java.util.HashMap;

public class Model {
    private Body bodyDown;
    private Body bodyUp;
    private Body bodyLeft;
    private Body bodyRight;

    private static AppPreferences preferences;
    private static HashMap<Body, Boolean> balls = new HashMap<>();
    private static Sound shotgun;
    private static Sound balloon;
    public static World world;

    private static final int FLOOR_POSITION_Y = -10;
    private static final int FLOOR_POSITION_X = 0;
    private static final int RIGHT_WALL_POSITION_X = 17;
    private static final int RIGHT_WALL_POSITION_Y = 0;
    private static final int LEFT_WALL_POSITION_X = -17;
    private static final int LEFT_WALL_POSITION_Y = 0;
    private static final int CEILING_POSITION_X = 0;
    private static final int CEILING_POSITION_Y = 13;

    public Body firstWrong;
    public Body secondWrong;
    public Body thirdWrong;
    public Body fourthWrong;
    public Body fifthWrong;
    public Body sixthWrong;
    public Body seventhWrong;
    public Body firstOkay;
    public Body secondOkay;
    public Body thirdOkay;

    public Model(ModelAssetManager assetManager) {
        preferences = new AppPreferences();
        world = new World(new Vector2(0, -10f), true);
        createFrame();

        assetManager.queueAddSounds();
        assetManager.assetManager.finishLoading();
        shotgun = assetManager.assetManager.get("sounds/shotgun.wav");
        balloon = assetManager.assetManager.get("sounds/balloon.wav");

        BodyFactory bodyFactory = BodyFactory.getInstance(world);

        firstWrong = BodyFactory.makeCirclePolyBody();
        secondWrong = BodyFactory.makeCirclePolyBody();
        thirdWrong = BodyFactory.makeCirclePolyBody();
        fourthWrong = BodyFactory.makeCirclePolyBody();
        fifthWrong = BodyFactory.makeCirclePolyBody();
        sixthWrong = BodyFactory.makeCirclePolyBody();
        seventhWrong = BodyFactory.makeCirclePolyBody();
        firstOkay = BodyFactory.makeCirclePolyBody();
        secondOkay = BodyFactory.makeCirclePolyBody();
        thirdOkay = BodyFactory.makeCirclePolyBody();

        balls.put(firstWrong, false);
        balls.put(secondWrong, false);
        balls.put(thirdWrong, false);
        balls.put(fourthWrong, false);
        balls.put(fifthWrong, false);
        balls.put(sixthWrong, false);
        balls.put(seventhWrong, false);
        balls.put(firstOkay, true);
        balls.put(secondOkay, true);
        balls.put(thirdOkay, true);
    }

    public void logicStep(float delta) {
        world.step(delta, 10, 10);
    }

    private void createFrame() {
        BodyDef bodyFloor = new BodyDef();
        bodyFloor.type = BodyDef.BodyType.StaticBody;
        bodyFloor.position.set(FLOOR_POSITION_X, FLOOR_POSITION_Y);

        BodyDef bodyRightWall = new BodyDef();
        bodyRightWall.type = BodyDef.BodyType.StaticBody;
        bodyRightWall.position.set(RIGHT_WALL_POSITION_X, RIGHT_WALL_POSITION_Y);

        BodyDef bodyLeftWall = new BodyDef();
        bodyLeftWall.type = BodyDef.BodyType.StaticBody;
        bodyLeftWall.position.set(LEFT_WALL_POSITION_X, LEFT_WALL_POSITION_Y);

        BodyDef bodyCeiling = new BodyDef();
        bodyCeiling.type = BodyDef.BodyType.StaticBody;
        bodyCeiling.position.set(CEILING_POSITION_X, CEILING_POSITION_Y);

        bodyDown = world.createBody(bodyFloor);
        bodyUp = world.createBody(bodyCeiling);
        bodyLeft = world.createBody(bodyLeftWall);
        bodyRight = world.createBody(bodyRightWall);

        createPolygon();
    }

    private void createPolygon() {
        PolygonShape horizontalShape = new PolygonShape();
        PolygonShape verticalShape = new PolygonShape();
        verticalShape.setAsBox(1, 200);
        horizontalShape.setAsBox(200, 1);
        bodyDown.createFixture(horizontalShape, 0.0f);
        bodyUp.createFixture(horizontalShape, 0.0f);
        bodyLeft.createFixture(verticalShape, 0.0f);
        bodyRight.createFixture(verticalShape, 0.0f);
        horizontalShape.dispose();
        verticalShape.dispose();
    }

    public static void removeBody(Vector2 mouseInWorld2D) {
        Array<Body> bodies = new Array<Body>();
        world.getBodies(bodies);
        for (Body body : bodies) {
            if (body.getType().equals(BodyDef.BodyType.DynamicBody)) {
                if (mouseInWorld2D.x <= (body.getPosition().x + 1.5) && mouseInWorld2D.x >= (body.getPosition().x - 1.5)) {
                    if (mouseInWorld2D.y <= (body.getPosition().y + 1.5) && mouseInWorld2D.y >= (body.getPosition().y - 1.5)) {
                        if (balls.get(body)) {
                            world.destroyBody(body);
                            body.setActive(false);
                            if (preferences.isSoundEffectsEnabled()) {
                                playSound();
                            }
                            body = BodyFactory.makeCirclePolyBody();
                            MainScreen.score++;
                        } else {
                            MainScreen.gameState = State.PAUSE;
                            Dialogs.defeatGameDialog(MainScreen.getSkin(), MainScreen.getStage());
                        }
                    }
                }
            }
        }
    }

    private static void playSound() {
        balloon.play(preferences.getSoundVolume());
        shotgun.play(preferences.getSoundVolume());
    }
}

