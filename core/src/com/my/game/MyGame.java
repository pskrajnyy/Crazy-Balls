package com.my.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.audio.Music;
import com.my.game.loader.ModelAssetManager;
import com.my.game.views.*;

public class MyGame extends Game {

	private LoadingScreen loadingScreen;
	private PreferencesScreen preferencesScreen;
	private MenuScreen menuScreen;
	private EndScreen endScreen;
	private HighscoreScreen rankingScreen;
	private DescriptionScreen descriptionScreen;
	private LotteryScreen lotteryScreen;
	private AppPreferences preferences;
	private Music playingSong;

	public ModelAssetManager assetManager = new ModelAssetManager();

	public static MainScreen mainScreen;

	public final static int MENU = 0;
	public final static int PREFERENCES = 1;
	public final static int APPLICATION = 2;
	public final static int ENDGAME = 3;
	public final static int RANKING = 4;
	public final static int DESCRIPTION = 5;
	public final static int LOTTERY = 6;

	@Override
	public void create () {
		loadingScreen = new LoadingScreen(this);
		preferences = new AppPreferences();
		setScreen(loadingScreen);

		assetManager.queueAddMusic();
		assetManager.assetManager.finishLoading();
		playingSong = assetManager.assetManager.get("music/Rolemusic_-_pl4y1ng.mp3");
		if(preferences.isMusicEnabled()) {
			playingSong.play();
			playingSong.setVolume(preferences.getMusicVolume());
		}
	}

	public void changeScreen(int screen){
		switch(screen){
			case MENU:
				if(menuScreen == null) menuScreen = new MenuScreen(this);
				this.setScreen(menuScreen);
				break;
			case LOTTERY:
				if (lotteryScreen == null) lotteryScreen = new LotteryScreen(this);
				this.setScreen(lotteryScreen);
				break;
			case PREFERENCES:
				if(preferencesScreen == null) preferencesScreen = new PreferencesScreen(this);
				this.setScreen(preferencesScreen);
				break;
			case APPLICATION:
				if(mainScreen == null) mainScreen = new MainScreen(this);
				this.setScreen(mainScreen);
				break;
			case ENDGAME:
				if(endScreen == null) endScreen = new EndScreen(this);
				this.setScreen(endScreen);
				break;
            case RANKING:
                if(rankingScreen == null) rankingScreen = new HighscoreScreen(this);
                this.setScreen(rankingScreen);
                break;
            case DESCRIPTION:
                if(descriptionScreen == null) descriptionScreen = new DescriptionScreen(this);
                this.setScreen(descriptionScreen);
                break;
		}
	}

	public AppPreferences getPreferences() {
		return this.preferences;
	}

	@Override
	public void dispose() {
		playingSong.dispose();
		assetManager.assetManager.dispose();
	}
}