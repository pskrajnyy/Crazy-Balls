package com.my.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

public class RenderScreen {

    public static void renderScreen(String title, int width, int height) {
        Gdx.gl.glClearColor(0f,  0f, 0f, 1);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.graphics.setTitle(title);
        Gdx.graphics.setWindowedMode(width, height);
        Gdx.graphics.setResizable(false);
    }
}
