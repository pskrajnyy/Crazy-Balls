package com.my.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;


public class TextureProvider {

    private static TextureRegion backgroundTexture;

    static {
        backgroundTexture = new TextureRegion(new Texture("images/otherBackground.jpg"), 0, 0, 1920, 1280);
    }

    public static TextureRegion getBackgroundTexture() {
        return backgroundTexture;
    }
}
