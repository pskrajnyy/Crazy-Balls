package com.my.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.my.game.MyGame;
import com.my.game.enums.State;
import com.my.game.views.MainScreen;

public class Dialogs {

    public static void exitGameFromMainDialog(Skin skin, Stage stage) {
        Gdx.input.setInputProcessor(stage);
        Dialog dialog = new Dialog("Exit", skin) {
            public void result(Object obj) {
                if (obj.equals(true)) {
                    Gdx.app.exit();
                } else {
                    Gdx.input.setInputProcessor(MainScreen.inputMultiplexer);
                    MainScreen.gameState = State.RUN;
                }
            }
        };
        dialog.setMovable(false);
        dialog.setColor(Color.BLUE);
        dialog.text("Are you sure you want to exit?");
        dialog.button("Cancel", false);
        dialog.button("Exit", true);
        dialog.show(stage);
    }

    public static void exitGameFromMenuDialog(Skin skin, Stage stage) {
        Gdx.input.setInputProcessor(stage);
        Dialog dialog = new Dialog("Exit", skin) {
            public void result(Object obj) {
                if (obj.equals(true)) {
                    Gdx.app.exit();
                }
            }
        };
        dialog.setMovable(false);
        dialog.setColor(Color.BLUE);
        dialog.text("Are you sure you want to exit?");
        dialog.button("Cancel", false);
        dialog.button("Exit", true);
        dialog.show(stage);
    }

    public static void defeatGameDialog(Skin skin, Stage stage) {
        Gdx.input.setInputProcessor(stage);
        Dialog dialog = new Dialog("Lose", skin) {
            public void result(Object obj) {
                if (obj.equals(true)) {
                    Gdx.app.exit();
                } else {
                    MainScreen.getParent().changeScreen(MyGame.MENU);
                    MainScreen.timeToLabel=0;
                    MainScreen.score=0;
                }
            }
        };
        dialog.setMovable(false);
        dialog.setColor(Color.BLUE);
        dialog.text("Lost ;( \n Do you want to start again?");
        dialog.button("Yes", false);
        dialog.button("No", true);
        dialog.show(stage);
    }
}
