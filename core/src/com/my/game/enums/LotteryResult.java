package com.my.game.enums;

public enum LotteryResult {
    WITH_ODD_NUMBER,
    WITH_EVEN_NUMBER,
    WITH_NUMBER_FROM_5_TO_8,
    WITH_NUMBER_FROM_1_TO_4,
    WITH_GREEN_COLOR,
    WITH_RED_COLOR,
    WITH_EVEN_NUMBER_AND_BLUE_COLOR
}
