package com.my.game.loader;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.assets.loaders.SkinLoader.SkinParameter;

public class ModelAssetManager {

    public final AssetManager assetManager = new AssetManager();

    public final String shotgunSound = "sounds/shotgun.wav";
    public final String balloonSound = "sounds/balloon.wav";
    public final String playingSong = "music/Rolemusic_-_pl4y1ng.mp3";
    public final String skin = "skin/uiskin.json";
    public final String gameImages = "images/game.atlas";

    public void queueAddImages(){
        assetManager.load(gameImages, TextureAtlas.class);
    }

    public void queueAddSkin(){
        SkinParameter params = new SkinParameter("skin/uiskin.atlas");
        assetManager.load(skin, Skin.class, params);
    }

    public void queueAddMusic(){
        assetManager.load(playingSong, Music.class);
    }

    public void queueAddSounds(){
        assetManager.load(shotgunSound, Sound.class);
        assetManager.load(balloonSound, Sound.class);
    }
}
