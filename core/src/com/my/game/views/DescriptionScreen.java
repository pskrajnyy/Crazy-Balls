package com.my.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.my.game.MyGame;
import com.my.game.RenderScreen;
import com.my.game.TextureProvider;

public class DescriptionScreen implements Screen {

    private MyGame parent;
    private SpriteBatch spriteBatch;
    private TextureRegion backgroundTexture;

    private static Stage stage;
    private static Skin skin;

    public DescriptionScreen(MyGame myGame) {
        parent = myGame;
        stage = new Stage(new ScreenViewport());
        spriteBatch = new SpriteBatch();
        spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
        backgroundTexture = TextureProvider.getBackgroundTexture();
        skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        makeTable();
    }

    @Override
    public void render(float delta) {
        RenderScreen.renderScreen("Instruction", 300, 450);

        spriteBatch.begin();
        spriteBatch.draw(backgroundTexture, 0, 0, 650, 500);
        spriteBatch.end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    private void makeTable() {
        Table table = new Table();
        table.setFillParent(true);
        stage.addActor(table);

        Label titleLabel = new Label("Instruction", skin, "title");
        Label description = new Label("The game consists in killing the appropriate balls. The game ends with the capture of 25 correct balls. Breaking the incomplete end of the game", skin);
        description.setWrap(true);
        Label controls = new Label("            Control:\n"
                + " Mouse - catching balls\n"
                + " P - Pausa\n"
                + " ESC - Exit", skin);
        description.setColor(Color.BLACK);
        controls.setColor(Color.BLACK);
        TextButton returnButton = new TextButton("Back", skin, "round");
        returnButton.setColor(Color.GOLD);

        table.add(titleLabel).center();
        table.row().width(200).pad(10, 0, 10, 0);
        table.add(description).fillX().uniformX();
        table.row().width(200).pad(10, 0, 10, 0);
        table.add(controls).fillX().uniformX();
        table.row().width(200).pad(10, 0, 10, 0);
        table.add(returnButton).fillX().uniformX();

        buttonListener(returnButton);
    }

    private void buttonListener(TextButton returnButton) {
        returnButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                parent.changeScreen(MyGame.MENU);
            }
        });
    }
}
