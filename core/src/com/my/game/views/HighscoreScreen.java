package com.my.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.my.game.AppPreferences;
import com.my.game.MyGame;
import com.my.game.RenderScreen;
import com.my.game.TextureProvider;

public class HighscoreScreen implements Screen {

    private MyGame parent;
    private SpriteBatch spriteBatch;
    private TextureRegion backgroundTexture;
    private AppPreferences preferences;

    private static Stage stage;
    private static Skin skin;

    public HighscoreScreen(MyGame myGame) {
        parent = myGame;
        stage = new Stage(new ScreenViewport());
        spriteBatch = new SpriteBatch();
        spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
        backgroundTexture = TextureProvider.getBackgroundTexture();
        skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
        preferences = new AppPreferences();
    }

    @Override
    public void show() {
        stage.clear();
        Gdx.input.setInputProcessor(stage);
        makeTable();
    }

    @Override
    public void render(float delta) {
        RenderScreen.renderScreen("Highscore", 300, 450);

        spriteBatch.begin();
        spriteBatch.draw(backgroundTexture, 0, 0, 600, 500);
        spriteBatch.end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    private void makeTable() {
        Table table = new Table();
        table.setFillParent(true);
        stage.addActor(table);

        Label titleLabel = new Label("Highscore", skin, "title");
        Label description = new Label("Player with nickname: " + preferences.getHighscorePlayer()
                                    + "\nhe obtained time: " + Integer.toString(preferences.getHighscore()) + " seconds", skin);
        description.setColor(Color.BLACK);

        TextButton returnButton = new TextButton("Back", skin, "round");

        table.add(titleLabel).center();
        table.row().pad(10,10,0,10);
        table.add(description).colspan(2).center();
        table.row().pad(10,10,0,10);
        table.add(returnButton).colspan(2).center();

        buttonListener(returnButton);
    }

    private void buttonListener(TextButton returnButton) {
        returnButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                parent.changeScreen(MyGame.MENU);
            }
        });
    }
}
