package com.my.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.my.game.MyGame;
import com.my.game.RenderScreen;

public class LoadingScreen implements Screen {

    private MyGame parent;
    private SpriteBatch spriteBatch;
    private TextureRegion backgroundTexture;
    private Label title;
    private ProgressBar loadingSlider;
    private float currentLoadingStage = 0;
    private float countDown = 1f;

    private static Stage stage;
    private static Skin skin;

    private final int SOUND = 0;
    private final int MUSIC = 1;

    public LoadingScreen(MyGame myGame) {
        parent = myGame;
        stage = new Stage(new ScreenViewport());
        spriteBatch = new SpriteBatch();
        spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
        backgroundTexture = new TextureRegion(new Texture("images/loadingBackground.jpg"), 0, 0, 640, 426);
        skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
        parent.assetManager.queueAddImages();
        parent.assetManager.assetManager.finishLoading();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        makeTable();
    }

    @Override
    public void render(float delta) {
        RenderScreen.renderScreen("Crazy balls", 640, 500);

        spriteBatch.begin();
        spriteBatch.draw(backgroundTexture, 0, 0, 640, 480);
        spriteBatch.end();

        stage.draw();
        loading(delta);
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    private void makeTable() {
        Table table = new Table();
        table.setFillParent(true);
        stage.addActor(table);

        title = new Label("Loading...", skin);
        title.setColor(Color.BLACK);

        loadingSlider = new ProgressBar(0f, 1f, 0.2f, false, skin);

        table.row().pad(260, 0, 10, 0);
        table.add(title).fillX().uniformX();
        table.row().pad(10, 0, 10, 0);
        table.add(loadingSlider);
    }

    private void loading(float delta) {
        if (parent.assetManager.assetManager.update()) {
            currentLoadingStage += 0.05;
            switch ((int) currentLoadingStage) {
                case SOUND:
                    title.setText("Loading sounds....");
                    loadingSlider.setValue(0.3f);
                    parent.assetManager.queueAddSounds();
                    break;
                case MUSIC:
                    title.setText("Loading musics....");
                    loadingSlider.setValue(0.6f);
                    parent.assetManager.queueAddMusic();
                    break;
                case 5:
                    title.setText("Loading complete");
                    loadingSlider.setValue(1f);
                    break;
            }
            if (currentLoadingStage > 2) {
                countDown -= delta;
                currentLoadingStage = 2;
                if (countDown < 0) {
                    parent.changeScreen(MyGame.MENU);
                }
            }
        }
    }
}