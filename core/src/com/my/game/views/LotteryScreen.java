package com.my.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.my.game.AppPreferences;
import com.my.game.RenderScreen;
import com.my.game.TextureProvider;
import com.my.game.enums.LotteryResult;
import com.my.game.MyGame;
import com.my.game.enums.State;

import java.util.*;

public class LotteryScreen implements Screen {

    private MyGame parent;
    private SpriteBatch spriteBatch;
    private TextureRegion backgroundTexture;
    private AppPreferences preferences;

    private static Stage stage;
    private static Skin skin;

    static LotteryResult result;

    public LotteryScreen(MyGame myGame) {
        parent = myGame;
        stage = new Stage(new ScreenViewport());
        spriteBatch = new SpriteBatch();
        spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
        backgroundTexture = TextureProvider.getBackgroundTexture();
        skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
        preferences = new AppPreferences();
    }

    @Override
    public void show() {
        stage.clear();
        Gdx.input.setInputProcessor(stage);
        makeTable();
    }

    @Override
    public void render(float delta) {
        RenderScreen.renderScreen("Crazy Balls", 300, 450);

        spriteBatch.begin();
        spriteBatch.draw(backgroundTexture, 0, 0, 650, 500);
        spriteBatch.end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    private void makeTable() {
        Table table = new Table();
        table.setFillParent(true);
        stage.addActor(table);

        Label title = new Label("Lottery", skin, "title");
        Label description = new Label("Your task is to collect the balls ", skin);
        description.setColor(Color.BLACK);
        Label lotteryResult = new Label(lottery(), skin);
        lotteryResult.setColor(Color.BLACK);
        Label textFieldDescription = new Label("Your nick: ", skin);
        textFieldDescription.setColor(Color.BLACK);
        Label warningPlayerName = new Label(null, skin);
        warningPlayerName.setColor(Color.BLACK);

        TextButton startButton = new TextButton("Start", skin, "round");
        startButton.setColor(Color.GOLD);
        TextButton returnButton = new TextButton("Back", skin, "round");
        returnButton.setColor(Color.BLUE);

        TextField playerName = new TextField(null, skin);

        table.add(title).center();
        table.row().pad(10, 0, 10, 0);
        table.add(description).fillX().uniformX();
        table.row().pad(10, 0, 10, 0);
        table.add(lotteryResult).fillX().uniformX();
        table.row().pad(10, 0, 10, 0);
        table.add(textFieldDescription).fillX().uniformX();
        table.row().pad(10, 0, 10, 0);
        table.add(playerName).fillX().uniformX();
        table.row().pad(10, 0, 10, 0);
        table.add(startButton).fillX().uniformX();
        table.row().pad(10, 0, 10, 0);
        table.add(returnButton).fillX().uniformX();
        table.row().pad(10, 0, 10, 0);
        table.add(warningPlayerName).fillX().uniformX();

        playButtonListener(warningPlayerName, startButton, playerName);
        returnButtonListener(returnButton);
    }

    private void playButtonListener(Label warningPlayerName, TextButton startButton, TextField playerName) {
        startButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if(playerName.getText().length() != 0) {
                    preferences.setHighscorePlayer(playerName.getText());
                    MainScreen.gameState= State.RUN;
                    parent.changeScreen(MyGame.APPLICATION);
                } else {
                    warningPlayerName.setText("You must give your nickname !!!");
                }
            }
        });
    }

    private void returnButtonListener(TextButton returnButton) {
        returnButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                parent.changeScreen(MyGame.MENU);
            }
        });
    }

    private String lottery() {
        Random random = new Random();
        result = LotteryResult.values()[random.nextInt(LotteryResult.values().length)];
        String resultLottery = result.toString();
        resultLottery = resultLottery.toLowerCase().replaceAll("_", " ");
        return resultLottery;
    }
}
