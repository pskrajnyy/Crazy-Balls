package com.my.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.my.game.RenderScreen;
import com.my.game.controller.Dialogs;
import com.my.game.enums.LotteryResult;
import com.my.game.enums.State;
import com.my.game.Model;
import com.my.game.MyGame;
import com.my.game.controller.KeyboardController;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MainScreen implements Screen {

    private Model model;
    private Box2DDebugRenderer debugRenderer;
    private OrthographicCamera cam;
    private KeyboardController controller;
    private AtlasRegion firstGood;
    private AtlasRegion secondGood;
    private AtlasRegion thirdGood;
    private AtlasRegion firstWrong;
    private AtlasRegion secondWrong;
    private AtlasRegion thirdWrong;
    private AtlasRegion fourthWrong;
    private AtlasRegion fifthWrong;
    private AtlasRegion sixthWrong;
    private AtlasRegion seventhWrong;
    private SpriteBatch spriteBatch;
    private TextureAtlas atlas;
    private LinkedList<Body> balls = new LinkedList<>();

    private static MyGame parent;
    private static Stage stage;
    private static Skin skin;
    private static Label timeLabel;
    private static Label scoreLabel;
    private static float time = 0;

    public static InputMultiplexer inputMultiplexer;
    public static State gameState;
    public static int score = 0;
    public static int timeToLabel = 0;


    public MainScreen(MyGame myGame) {
        parent = myGame;
        stage = new Stage(new ScreenViewport());
        skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
        cam = new OrthographicCamera(32, 24);
        controller = new KeyboardController(cam);
        model = new Model(parent.assetManager);
        debugRenderer = new Box2DDebugRenderer(true, true, true, true, true, true);
        gameState = State.RUN;
        spriteBatch = new SpriteBatch();
        spriteBatch.setProjectionMatrix(cam.combined);
    }

    @Override
    public void show() {
        stage.clear();
        InputProcessor inputProcessorOne = controller;
        InputProcessor inputProcessorTwo = stage;
        inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(inputProcessorOne);
        inputMultiplexer.addProcessor(inputProcessorTwo);
        Gdx.input.setInputProcessor(inputMultiplexer);
        filterAtlas();
        makeTable();
    }

    @Override
    public void render(float delta) {
        scoreLabel.setText("Score: " + score);
        checkScore();
        model.logicStep(delta);
        RenderScreen.renderScreen("Crazy balls", 800, 600);
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        if(gameState.equals(State.RUN)) {
            timer(delta);
        }
        checkStateGame();
        spriteBatch.begin();
        checkActiveModel();
        spriteBatch.end();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    public static Stage getStage() {
        return stage;
    }

    public static Skin getSkin() {
        return skin;
    }

    public static MyGame getParent() {
        return parent;
    }

    public static void exit() {
        Dialogs.exitGameFromMainDialog(skin, stage);
    }

    private void checkScore() {
        if(score==25) {
            score=0;
            gameState = State.PAUSE;
            parent.changeScreen(MyGame.ENDGAME);
            timeToLabel=0;
        }
    }

    private void checkStateGame() {
        if(gameState.equals(State.PAUSE)) {
            addBallsToList();
            for(Body ball: balls) {
                ball.setType(BodyDef.BodyType.StaticBody);
            }
        } else if(gameState.equals(State.RUN)) {
            for(Body ball: balls) {
                ball.setType(BodyDef.BodyType.DynamicBody);
            }
        }
    }

    private void addBallsToList() {
        balls.add(model.firstOkay);
        balls.add(model.secondOkay);
        balls.add(model.thirdOkay);
        balls.add(model.firstWrong);
        balls.add(model.secondWrong);
        balls.add(model.thirdWrong);
        balls.add(model.fourthWrong);
        balls.add(model.fifthWrong);
        balls.add(model.fourthWrong);
        balls.add(model.sixthWrong);
        balls.add(model.seventhWrong);
    }

    private void checkActiveModel() {
        if (model.firstOkay.isActive()) {
            spriteBatch.draw(firstGood, model.firstOkay.getPosition().x - 2, model.firstOkay.getPosition().y - 2, 4, 4);
        }
        if (model.secondOkay.isActive()) {
            spriteBatch.draw(secondGood, model.secondOkay.getPosition().x - 2, model.secondOkay.getPosition().y - 2, 4, 4);
        }
        if (model.thirdOkay.isActive()) {
            spriteBatch.draw(thirdGood, model.thirdOkay.getPosition().x - 2, model.thirdOkay.getPosition().y - 2, 4, 4);
        }
        if (model.firstWrong.isActive()) {
            spriteBatch.draw(firstWrong, model.firstWrong.getPosition().x - 2, model.firstWrong.getPosition().y - 2, 4, 4);
        }
        if (model.secondWrong.isActive()) {
            spriteBatch.draw(secondWrong, model.secondWrong.getPosition().x - 2, model.secondWrong.getPosition().y - 2, 4, 4);
        }
        if (model.thirdWrong.isActive()) {
            spriteBatch.draw(thirdWrong, model.thirdWrong.getPosition().x - 2, model.thirdWrong.getPosition().y - 2, 4, 4);
        }
        if (model.fourthWrong.isActive()) {
            spriteBatch.draw(fourthWrong, model.fourthWrong.getPosition().x - 2, model.fourthWrong.getPosition().y - 2, 4, 4);
        }
        if (model.fifthWrong.isActive()) {
            spriteBatch.draw(fifthWrong, model.fifthWrong.getPosition().x - 2, model.fifthWrong.getPosition().y - 2, 4, 4);
        }
        if (model.sixthWrong.isActive()) {
            spriteBatch.draw(sixthWrong, model.sixthWrong.getPosition().x - 2, model.sixthWrong.getPosition().y - 2, 4, 4);
        }
        if (model.seventhWrong.isActive()) {
            spriteBatch.draw(seventhWrong, model.seventhWrong.getPosition().x - 2, model.seventhWrong.getPosition().y - 2, 4, 4);
        }
    }

    private void filterAtlas() {
        atlas = parent.assetManager.assetManager.get("images/game.atlas");
        LotteryResult lotteryResult = LotteryScreen.result;
        String searchGoodBalls = null;
        String searchBadBalls = null;

        switch (lotteryResult) {
            case WITH_ODD_NUMBER: {
                searchBadBalls = "lp";
                searchGoodBalls = "ln";
                break;
            }
            case WITH_EVEN_NUMBER: {
                searchGoodBalls = "lp";
                searchBadBalls = "ln";
                break;
            }
            case WITH_NUMBER_FROM_5_TO_8: {
                searchGoodBalls = "t58";
                searchBadBalls = "n58";
                break;
            }
            case WITH_NUMBER_FROM_1_TO_4: {
                searchGoodBalls = "t14";
                searchBadBalls = "n14";
                break;
            }
            case WITH_GREEN_COLOR: {
                searchGoodBalls = "tz";
                searchBadBalls = "nz";
                break;
            }
            case WITH_RED_COLOR: {
                searchGoodBalls = "tc";
                searchBadBalls = "nc";
                break;
            }
            case WITH_EVEN_NUMBER_AND_BLUE_COLOR: {
                searchGoodBalls = "tnp";
                searchBadBalls = "nnp";
                break;
            }
        }

        List<AtlasRegion> goodRegions = searchRegions(searchGoodBalls);
        List<AtlasRegion> badRegions = searchRegions(searchBadBalls);
        setTexture(goodRegions, badRegions);
    }

    private void setTexture(List<AtlasRegion> goodRegions, List<AtlasRegion> badRegions) {
        firstGood = atlas.findRegion(getRandomGoodBall(goodRegions));
        secondGood = atlas.findRegion(getRandomGoodBall(goodRegions));
        thirdGood = atlas.findRegion(getRandomGoodBall(goodRegions));
        firstWrong = atlas.findRegion(getRandomBadBall(badRegions));
        secondWrong = atlas.findRegion(getRandomBadBall(badRegions));
        thirdWrong = atlas.findRegion(getRandomBadBall(badRegions));
        fourthWrong = atlas.findRegion(getRandomBadBall(badRegions));
        fifthWrong = atlas.findRegion(getRandomBadBall(badRegions));
        sixthWrong = atlas.findRegion(getRandomBadBall(badRegions));
        seventhWrong = atlas.findRegion(getRandomBadBall(badRegions));
    }

    private List<AtlasRegion> searchRegions(String search) {
        Array<AtlasRegion> findInAtlas = atlas.getRegions();
        return StreamSupport.stream(findInAtlas.spliterator(), false)
                .filter(region -> region.name.contains(search))
                .collect(Collectors.toList());
    }

    private String getRandomGoodBall(List<AtlasRegion> goodRegions) {
        Random random = new Random();
        return goodRegions.get(random.nextInt(goodRegions.size())).toString();
    }

    private String getRandomBadBall(List<AtlasRegion> badRegions) {
        Random random = new Random();
        return badRegions.get(random.nextInt(badRegions.size())).toString();
    }

    private void makeTable() {
        Table table = new Table();
        table.setPosition(400,30);
        stage.addActor(table);

        timeLabel = new Label("Time: 0 s", skin);
        scoreLabel = new Label("Score: 0", skin);

        table.row().pad(10,0,10,0);
        table.add(timeLabel).center();
        table.row().pad(10, 0, 10, 0);
        table.add(scoreLabel).center();
    }

    private void timer(float delta) {
        time = time + delta;
        if (time > 1) {
            timeToLabel++;
            timeLabel.setText("Time: " + timeToLabel + " seconds");
            time = 0;
        }
    }
}
