package com.my.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.my.game.MyGame;
import com.my.game.RenderScreen;
import com.my.game.TextureProvider;
import com.my.game.controller.Dialogs;

public class MenuScreen implements Screen {

    private MyGame parent;
    private SpriteBatch spriteBatch;
    private TextureRegion backgroundTexture;

    private static Stage stage;
    private static Skin skin;

    public MenuScreen(MyGame myGame) {
        parent = myGame;
        stage = new Stage(new ScreenViewport());
        spriteBatch = new SpriteBatch();
        spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
        backgroundTexture = TextureProvider.getBackgroundTexture();
        skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
        parent.assetManager.queueAddSkin();
        parent.assetManager.assetManager.finishLoading();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        makeTable();
    }

    @Override
    public void render(float delta) {
        RenderScreen.renderScreen("Main menu", 300, 450);
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));

        spriteBatch.begin();
        spriteBatch.draw(backgroundTexture, 0, 0,650,500);
        spriteBatch.end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    private void makeTable() {
        Table table = new Table();
        table.setFillParent(true);
        stage.addActor(table);

        Label title = new Label("Main menu", skin, "title");

        TextButton newGame = new TextButton("New game", skin, "round");
        newGame.setColor(Color.GREEN);
        TextButton ranking = new TextButton("Highscore", skin, "round");
        ranking.setColor(Color.GRAY);
        TextButton description = new TextButton("Instruction", skin, "round");
        description.setColor(Color.ORANGE);
        TextButton preferences = new TextButton("Preferences", skin, "round");
        preferences.setColor(Color.MAGENTA);
        TextButton exit = new TextButton("Exit", skin, "round");
        exit.setColor(Color.GOLD);

        table.row().pad(10, 0, 10, 0);
        table.add(title).fillX().uniformX();
        table.row().pad(10, 0, 10, 0);
        table.add(newGame).fillX().uniformX();
        table.row().pad(10, 0, 10, 0);
        table.add(ranking).fillX().uniformX();
        table.row().pad(10, 0, 10, 0);
        table.add(description).fillX().uniformX();
        table.row().pad(10, 0, 10, 0);
        table.add(preferences).fillX().uniformX();
        table.row().pad(10, 0, 10, 0);
        table.add(exit).fillX().uniformX();

        buttonListener(newGame, MyGame.LOTTERY);
        buttonListener(ranking, MyGame.RANKING);
        buttonListener(description, MyGame.DESCRIPTION);
        buttonListener(preferences, MyGame.PREFERENCES);
        exitListener(exit);
    }

    private void buttonListener(TextButton textButton, int screen) {
        textButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                parent.changeScreen(screen);
            }
        });
    }

    private void exitListener(TextButton exit) {
        exit.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Dialogs.exitGameFromMenuDialog(skin, stage);
            }
        });
    }
}