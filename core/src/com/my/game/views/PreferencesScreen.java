package com.my.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.my.game.MyGame;
import com.my.game.RenderScreen;
import com.my.game.TextureProvider;

public class PreferencesScreen implements Screen{

    private MyGame parent;
    private SpriteBatch spriteBatch;
    private TextureRegion backgroundTexture;

    private static Stage stage;
    private static Skin skin;

    public PreferencesScreen(MyGame myGame){
        parent = myGame;
        stage = new Stage(new ScreenViewport());
        spriteBatch = new SpriteBatch();
        spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
        backgroundTexture = TextureProvider.getBackgroundTexture();
        skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);

        Table table = new Table();
        table.setFillParent(true);
        stage.addActor(table);

        final Slider volumeMusicSlider = volumeMusicSlider();
        final Slider soundMusicSlider = soundMusicSlider();
        final Button musicButton = musicButton();
        final Button soundEffectsButton = soundEffectsButton();
        final TextButton backButton = backButton();

        Label titleLabel = new Label("Preferences", skin, "title");
        Label volumeMusicLabel = new Label("Music volume", skin);
        volumeMusicLabel.setColor(Color.BLACK);
        Label volumeSoundLabel = new Label("Sounds volume", skin);
        volumeSoundLabel.setColor(Color.BLACK);
        Label musicOnOffLabel = new Label("Music", skin);
        musicOnOffLabel.setColor(Color.BLACK);
        Label soundOnOffLabel = new Label("Sounds", skin);
        soundOnOffLabel.setColor(Color.BLACK);


        table.add(titleLabel).colspan(2);
        makeTable(table, volumeMusicSlider, musicButton, volumeMusicLabel, musicOnOffLabel);
        makeTable(table, soundMusicSlider, soundEffectsButton, volumeSoundLabel, soundOnOffLabel);
        table.row().pad(10,0,0,10);
        table.add(backButton).colspan(2);

    }

    @Override
    public void render(float delta) {
        RenderScreen.renderScreen("Preferences", 300, 450);
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));

        spriteBatch.begin();
        spriteBatch.draw(backgroundTexture, 0, 0, 650, 500);
        spriteBatch.end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    private void makeTable(Table table, Slider soundMusicSlider, Button soundEffectsCheckbox, Label volumeSoundLabel, Label soundOnOffLabel) {
        table.row().pad(10,0,0,10);
        table.add(volumeSoundLabel).left();
        table.add(soundMusicSlider);
        table.row().pad(10,0,0,10);
        table.add(soundOnOffLabel).left();
        table.add(soundEffectsCheckbox);
    }

    private TextButton backButton() {
        final TextButton backButton = new TextButton("Back", skin, "round");
        backButton.setColor(Color.GOLD);
        backButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                parent.changeScreen(MyGame.MENU);

            }
        });
        return backButton;
    }

    private Button soundEffectsButton() {
        final Button soundEffectsButton = new Button(null, skin, "sound");
        soundEffectsButton.setChecked(parent.getPreferences().isSoundEffectsEnabled());
        soundEffectsButton.addListener(new EventListener() {
            @Override
            public boolean handle(Event event) {
                boolean enabled = soundEffectsButton.isChecked();
                parent.getPreferences().setSoundEffectsEnabled(enabled);
                return false;
            }
        });
        return soundEffectsButton;
    }

    private Button musicButton() {
        final Button musicButton = new Button(null, skin, "music");
        musicButton.setChecked(parent.getPreferences().isMusicEnabled());
        musicButton.addListener(new EventListener() {
            @Override
            public boolean handle(Event event) {
                boolean enabled = musicButton.isChecked();
                parent.getPreferences().setMusicEnabled(enabled);
                return false;
            }
        });
        return musicButton;
    }

    private Slider soundMusicSlider() {
        final Slider soundMusicSlider = new Slider(0f, 1f, 0.1f, false, skin);
        soundMusicSlider.setValue(parent.getPreferences().getSoundVolume());
        soundMusicSlider.addListener(new EventListener() {
            @Override
            public boolean handle(Event event) {
                parent.getPreferences().setSoundVolume(soundMusicSlider.getValue());
                return false;
            }
        });
        return soundMusicSlider;
    }

    private Slider volumeMusicSlider() {
        final Slider volumeMusicSlider = new Slider(0f, 1f, 0.1f, false, skin);
        volumeMusicSlider.setValue(parent.getPreferences().getMusicVolume());
        volumeMusicSlider.addListener(new EventListener() {
            @Override
            public boolean handle(Event event) {
                parent.getPreferences().setMusicVolume(volumeMusicSlider.getValue());
                return false;
            }
        });
        return volumeMusicSlider;
    }
}
